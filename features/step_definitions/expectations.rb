Then(/^I should see cost \$ "([^"]*)" in Print$/) do |print|
  expect(page).to have_css("#valueCounterMediaPrint", :text => print)
end

Then(/^I should see cost \$ "([^"]*)" in Radio$/) do |radio|
  expect(page).to have_css("#valueCounterMediaRadio", :text => radio)
end

Then(/^I should see cost \$ "([^"]*)" in TV$/) do |tv|
  expect(page).to have_css("#valueCounterMediaTV", :text => tv)
end

Then(/^I should see cost \$ "([^"]*)" in Outdoor$/) do |outdoor|
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => outdoor)
end

Then(/^I should see "([^"]*)" in Cost \$$/) do |cost|
  within(:xpath, '//div[@id="content-widgets-4-0"]') do
    expect(page).to have_content(cost)
  end
end

And(/^I should see "([^"]*)" in Previous Period Cost \$$/) do |ppcost|
  within(:xpath, '//div[@id="content-widgets-4-0"]') do
    expect(page).to have_content(ppcost)
  end
end

And(/^I should see "([^"]*)" in Clicks$/) do |clicks|
  within(:xpath, '//div[@id="content-widgets-4-1"]') do
    expect(page).to have_content(clicks)
  end
end

And(/^I should see "([^"]*)" in Previous Period Clicks$/) do |ppclicks|
  within(:xpath, '//div[@id="content-widgets-4-1"]') do
    expect(page).to have_content(ppclicks)
  end
end

And(/^I should see "([^"]*)" in Cost Per Click \$$/) do |cpc|
  within(:xpath, '//div[@id="content-widgets-4-2"]') do
    expect(page).to have_content(cpc)
  end
end

And(/^I should see "([^"]*)" in Previous Period Cost Per Click \$$/) do |ppcpc|
  within(:xpath, '//div[@id="content-widgets-4-2"]') do
    expect(page).to have_content(ppcpc)
  end
end

And(/^I should see "([^"]*)" in Click-Through rate %$/) do |ctr|
  within(:xpath, '//div[@id="content-widgets-4-3"]') do
    expect(page).to have_content(ctr)
  end
end

And(/^I should see "([^"]*)" in Previous Period Click-Through rate %$/) do |ppctr|
  within(:xpath, '//div[@id="content-widgets-4-3"]') do
    expect(page).to have_content(ppctr)
  end
end

And(/^I should see "([^"]*)" in Total Spend \$$/) do |totalspend|
  within(:xpath, '//span[@id="valueCounterBarCountTOTAL SPEND"]') do
    expect(page).to have_content(totalspend)
  end
end

And(/^I should see "([^"]*)" in Total Leads$/) do |totalleads|
  within(:xpath, '//span[@id="valueCounterBarCountTOTAL LEADS"]') do
    expect(page).to have_content(totalleads)
  end
end

And(/^I should see "([^"]*)" in Total CPL \$$/) do |totalcpl|
  within(:xpath, '//span[@id="valueCounterBarCountTOTAL CPL"]') do
    expect(page).to have_content(totalcpl)
  end
end