Given (/^PLS AUTO website$/) do
  visit "https://qa.plsauto.com/"
end

And (/^I should see all UI objects$/) do
  within(:css, ".flex-row.flex-between-center") do
    expect(page).to have_content("TYPE")
    expect(page).to have_content("MAKE")
    expect(page).to have_content("ALL CARS")
    expect(page).to have_xpath("//*[@id='root']/div/div[1]/div[1]/div/div/a/div/img") #logo
    expect(page).to have_xpath("//*[@id='root']/div/div[1]/div[1]/div/div/div[2]/div[1]/button") #search button
    expect(page).to have_xpath("//*[@id='root']/div/div[1]/div[1]/div/div/div[2]/div[2]") #location icon
    expect(page).to have_xpath("//*[@id='root']/div/div[1]/div[1]/div/div/div[2]/div[3]") #phone icon
    expect(page).to have_xpath("//*[@id='root']/div/div[1]/div[1]/div/div/div[2]/div[4]") #working hours icon
  end
  expect(page).to have_xpath("//*[@id='root']/div/div[2]/div/div[1]/div/section/article/section/section/div/div[1]/div[1]") #slider
  expect(page).to have_xpath("//*[@id='root']/div/div[2]/div/div[2]/a/img[1]") #deal
  expect(page).to have_xpath("//*[@id='map']/div[2]/div") #dealership & service hours
  expect(page).to have_xpath("//*[@id='map']/div[1]/div/div[1]/div/div/div[1]/div[3]") #map
  expect(page).to have_content("Call Us ")
  expect(page).to have_content("Visit Our Location")
  expect(page).to have_content("About Us")
  expect(page).to have_content("Terms")
  expect(page).to have_content("Privacy")
  expect(page).to have_xpath("//*[@id='root']/div/div[2]/div/div[4]/div/div[3]/span") #Service mark notice
end

When (/^I click "([^"]*)"$/) do |button|
  click_button(button)
end

And (/^I select Mid-Size MPV$/) do
  find(:xpath, "//*[@id='root']/div/div[1]/div[4]/div[1]/div/div[5]/div/span[1]").click
end

And (/^I select Toyota$/) do
  find(:xpath, "//*[@id='root']/div/div[1]/div[4]/div[1]/div/div[3]/div[3]").click
end

Then (/^I should see Mid-size MPV cars$/) do
  find(".searchResultTitle", match: :first).click #click on first element from result list
  expect(page).to have_content("MID-SIZE MPV")
end

Then (/^I should see Toyota cars$/) do
  expect(page).to have_content "Toyota"
  find(".searchResultTitle", match: :first).click #click on first element from result list
  expect(page).to have_content "Toyota"
end

When (/^I search Toyota$/) do
  find(:xpath, "//*[@id='root']/div/div[1]/div[1]/div/div/div[2]/div[1]/button").click
  find(:xpath, "//*[contains(@id,'undefined-SearchCars-undefined')]").native.send_keys('Toyota', :enter)
end
