##### HEALTH CARE SUCCESS #####

When(/^I Log In with login "([^"]*)" and password "([^"]*)"$/) do |login,password| #HS login
  fill_in('user', :with => login)
  fill_in('pass', :with => password)
  click_button('Log In')
end

And(/^I set date range from "([^"]*)" to "([^"]*)"$/) do |daterange_start,daterange_end| #set date range with dashboard date picker
  #expect(page).to have_selector("#reportrange", visible: true)
  within(:css,'#header-widgets-0-2') do
  page.find(:css,"#reportrange").click
  end
  page.find(:css,".ranges.timeFrameGeneral>ul>li", :text => 'Custom Range').click
  fill_in('daterangepicker_start', :match => :first, :with => daterange_start)
  fill_in('daterangepicker_end', :match => :first, :with => '')
  fill_in('daterangepicker_end', :match => :first, :with => daterange_end)
  page.find(:css,".applyBtn.btn.btn-sm.timeframe-btn-apply").click
end


Then(/^I should see Logo and navigation bar$/) do
  expect(page).to have_css("img[src*='SB-logo.png']")
  expect(page).to have_selector('#pdfgenerator', :text => 'DOWNLOAD PDF', visible: true)
  within(:css, '#headerNavigationContainer') do
    expect(page).to have_selector('#launchLogoutCallback', :text => 'LOGOUT', visible: true)
  end
end

Then(/^I should see Title and date selector$/) do
  expect(page).to have_selector(".mainTitleText.mainTitleTextPosition", visible: true)
  expect(page).to have_selector("#reportrange", visible: true)
  expect(page).to have_css(".timeFrameLastPeriodText.timeFrameLastPeriodTextPosition", visible: true)
end

Then(/^I should see summary widgets$/) do
  expect(page).to have_css('#pdfgenerator', :text => 'DOWNLOAD PDF', visible: true) #pdf loads with summary widgets
  expect(page).to have_css('.singleValueBarTitle', :text => 'TOTAL SPEND', visible: true)
  expect(page).to have_css('.singleValueBarTitle', :text => 'TOTAL LEADS', visible: true)
  expect(page).to have_css('.singleValueBarTitle', :text => 'TOTAL CPL', visible: true)
  expect(page).to have_xpath("//*[@id='valueCounterBarCountTOTAL SPEND']", visible: true)
  expect(page).to have_xpath("//*[@id='valueCounterBarCountTOTAL LEADS']", visible: true)
  expect(page).to have_xpath("//*[@id='valueCounterBarCountTOTAL CPL']", visible: true)

end

Then(/^I should see Paid online media$/) do
  expect(page).to have_css('.titleTextPosition', :text => 'PAID ONLINE MEDIA', visible: true)
  expect(page).to have_css('.widget-component.widget-padding.widget-right-spacing.widget-bottom-spacing', visible: true)
  expect(page).to have_css('.numbers.singlevaluetrendnumbers', visible: true)
  expect(page).to have_css('.value.singlevaluetrendmaincontenttext.singlevaluetrend-mainContent', visible: true)
  expect(page).to have_css('.value.singlevaluetrendsecondarytitletext.singlevaluetrend-secondaryTitle', visible: true)
  expect(page).to have_css('.graph.singlevaluetrendgraphdiv>canvas', visible: true)
end

Then(/^I should see Total conversions$/) do
  expect(page).to have_css('.pieChartPreviousWidgetContainer', visible: true)
  page.find(:css,"#conversionsCanvas").click
  within(:css, ".table-row.level-0") do
    expect(page).to have_css('.table-cell', :text => 'Date')
    expect(page).to have_css('.table-cell', :text => 'Starting Time')
    expect(page).to have_css('.table-cell', :text => 'Forward Number')
    expect(page).to have_css('.table-cell', :text => 'Caller Number')
    expect(page).to have_css('.table-cell', :text => 'Ad Source')
    expect(page).to have_css('.table-cell', :text => 'Duration')
    expect(page).to have_css('.table-cell', :text => 'Audio')
  end
  page.find(:css,".close-mymodal").click
end

Then(/^I should see Impressions$/) do
  expect(page).to have_selector('#StackedDoughnut', visible: true)
end

Then(/^I should see Performance metrics$/) do
  expect(page).to have_selector('#content-widgets-ROW-5', visible: true)
  expect(page).to have_css('.titleTextPosition', :text => 'Month Over Month Performance')
end

Then(/^I should see Facebook ads$/) do
  expect(page).to have_css('.titleTextPosition', :text => 'FACEBOOK ADS')
  expect(page).to have_selector('#content-widgets-14-0', visible: true)
  expect(page).to have_selector('#content-widgets-14-1', visible: true)
  expect(page).to have_selector('#content-widgets-14-2', visible: true)
  expect(page).to have_selector('#content-widgets-14-3', visible: true)
end

Then(/^I should see Offline media$/) do
  expect(page).to have_css('.titleTextPosition', :text => 'OFFLINE MEDIA')
  expect(page).to have_css('.titleTextPosition', :text => 'COST PER MEDIA')
  expect(page).to have_css('.sectionTitleContainer', :text => 'Print')
  expect(page).to have_selector('#content-widgets-20-0', visible: true)
  expect(page).to have_css('.svmc-icon.icon-Print')
  expect(page).to have_css('.sectionTitleContainer', :text => 'Radio')
  expect(page).to have_selector('#content-widgets-20-1', visible: true)
  expect(page).to have_css('.svmc-icon.icon-Radio')
  expect(page).to have_css('.sectionTitleContainer', :text => 'TV')
  expect(page).to have_selector('#content-widgets-20-2', visible: true)
  expect(page).to have_css('.svmc-icon.icon-TV')
  expect(page).to have_css('.sectionTitleContainer', :text => 'Outdoor')
  expect(page).to have_selector('#content-widgets-20-3', visible: true)
  expect(page).to have_css('.svmc-icon.icon-Bilboard')
end

Then(/^I should see Unique Calls wheel$/) do
  expect(page).to have_css('.title1', :text => 'Unique Calls By Source')
  expect(page).to have_selector('#campaignBreakdownWheel', visible: true)
  expect(page).to have_css('.title2', :text => 'Campaign Breakdown')
  expect(page).to have_selector('#legendColumnWrapper', visible: true)
end


##### MORAN #####

When (/^I select client "([^"]*)" and select date range from "([^"]*)" to "([^"]*)" $/) do |client_name,start_date,end_date|
  select(client_name, :from => 'client_select')
  fill_in('startDate', :with => start_date)
  fill_in('endDate', :with => end_date)
  select('Option 1', :from => 'option_select')
  click_button('VIEW DASHBOARD')
end

#####


And(/^I click on invoca detail view$/) do
  expect(page).to have_css('.pieChartPreviousWidgetContainer', visible: true)
  page.find(:css,"#conversionsCanvas").click
end

Then(/^I should see detail view$/) do
  within(:css, ".table-row.level-0") do
    expect(page).to have_css('.table-cell', :text => 'Date')
    expect(page).to have_css('.table-cell', :text => 'Starting Time')
    expect(page).to have_css('.table-cell', :text => 'Forward Number')
    expect(page).to have_css('.table-cell', :text => 'Caller Number')
    expect(page).to have_css('.table-cell', :text => 'Ad Source')
    expect(page).to have_css('.table-cell', :text => 'Duration')
    expect(page).to have_css('.table-cell', :text => 'Audio')
  end
  page.find(:css,".close-mymodal").click
end

Then(/^I should see calls in detail view$/) do
  expect(page).to have_content('Date')
  expect(page).to have_content('Starting Time')
  expect(page).to have_content('Forward Number')
  expect(page).to have_content('Caller Number')
  expect(page).to have_content('Ad Source')
  expect(page).to have_content('Duration')
  expect(page).to have_content('Audio')
  expect(page).to have_content('815-981-4742')
  page.find(:css,".close-mymodal").click
end

When(/^I Log In with wrong credentials$/) do
  fill_in('user', :with =>'aaa')
  fill_in('pass', :with =>'aaa')
  click_button('Log In')
end

And(/^I see login page elements$/) do
  expect(page).to have_content('Username')
  expect(page).to have_content('Password')
  expect(page).to have_content('Forgot Password?')
  expect(page).to have_selector("#submit")
end


Then(/^I should see error message$/) do
  expect(page).to have_css('.error', :text => 'Oops… username or password incorrect')
end


And (/^I open "([^"]*)" account$/) do |account_name|
  select(account_name, :from => 'client_select')
end

And (/^I set range data from "([^"]*)" to "([^"]*)"$/) do |start_date,end_date|
  fill_in('startDate', :with => start_date)
  fill_in('endDate', :with => end_date)
  select('Option 1', :from => 'option_select')
  click_button('VIEW DASHBOARD')
end

Then(/^I should see the total count$/) do
  # assumes all existing products are in the cart
  total = Product.find(:all).inject { |sum, p| sum + p.price }
  page.find(".total").should have_content number_to_currency(total)
end

