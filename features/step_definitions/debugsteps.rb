############   SIMPLE CODE   ##########################
Then (/^I should see offline media prices for January$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "850")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,150")
  expect(page).to have_css("#valueCounterMediaTV", :text => "1,810")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "610")
end

Then (/^I should see offline media prices for February$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "730")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,160")
  expect(page).to have_css("#valueCounterMediaTV", :text => "1,990")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "600")
end

Then (/^I should see offline media prices for March$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "600")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,250")
  expect(page).to have_css("#valueCounterMediaTV", :text => "2,510")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "540")
end

Then (/^I should see offline media prices for April$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "950")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,330")
  expect(page).to have_css("#valueCounterMediaTV", :text => "3,000")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "456")
end

Then (/^I should see offline media prices for May$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "821")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,543")
  expect(page).to have_css("#valueCounterMediaTV", :text => "2,210")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "789")
end

Then (/^I should see offline media prices for June$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "840")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,234")
  expect(page).to have_css("#valueCounterMediaTV", :text => "1,710")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "678")
end

Then (/^I should see offline media prices for July$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "660")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "950")
  expect(page).to have_css("#valueCounterMediaTV", :text => "2,770")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "555")
end

Then (/^I should see offline media prices for August$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "899")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "2,000")
  expect(page).to have_css("#valueCounterMediaTV", :text => "1,933")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "777")
end

Then (/^I should see offline media prices for September$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "950")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "2,000")
  expect(page).to have_css("#valueCounterMediaTV", :text => "1,989")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "1,234")
end

Then (/^I should see offline media prices for October$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "670")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,150")
  expect(page).to have_css("#valueCounterMediaTV", :text => "2,016")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "1,111")
end

Then (/^I should see offline media prices for November$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "777")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,750")
  expect(page).to have_css("#valueCounterMediaTV", :text => "2,230")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "700")
end

Then (/^I should see offline media prices for December$/) do
  expect(page).to have_css("#valueCounterMediaPrint", :text => "850")
  expect(page).to have_css("#valueCounterMediaRadio", :text => "1,109")
  expect(page).to have_css("#valueCounterMediaTV", :text => "1,210")
  expect(page).to have_css("#valueCounterMediaOutdoor", :text => "800")
end
