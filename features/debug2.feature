Feature: Offline Data Validator v2

  @selenium @debug2
Scenario: Offline media validation
Given HS Login Page
When I Log In with login "admin" and password "admin123"
And I set date range from "01/01/2016" to "01/31/2016"
Then I should see offline media prices for January
When I set date range from "02/01/2016" to "02/29/2016"
Then I should see offline media prices for February
When I set date range from "03/01/2016" to "03/31/2016"
Then I should see offline media prices for March
When I set date range from "04/01/2016" to "04/30/2016"
Then I should see offline media prices for April
When I set date range from "05/01/2016" to "05/31/2016"
Then I should see offline media prices for May
When I set date range from "06/01/2016" to "06/30/2016"
Then I should see offline media prices for June
When I set date range from "07/01/2016" to "07/31/2016"
Then I should see offline media prices for July
When I set date range from "08/01/2016" to "08/31/2016"
Then I should see offline media prices for August
When I set date range from "09/01/2016" to "09/30/2016"
Then I should see offline media prices for September
When I set date range from "10/01/2016" to "10/31/2016"
Then I should see offline media prices for October
When I set date range from "11/01/2016" to "11/30/2016"
Then I should see offline media prices for November
When I set date range from "12/01/2016" to "12/31/2016"
Then I should see offline media prices for December



