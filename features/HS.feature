Feature: Smoke test

  @selenium @validation @ready
  Scenario: UI Validation
    Given HS Login Page
    When I Log In with login "admin" and password "admin123"
    Then I should see Logo and navigation bar
    And I should see Title and date selector
    And I should see summary widgets
    And I should see Paid online media
    And I should see Total conversions
    And I should see Impressions
    And I should see Performance metrics
    And I should see Facebook ads
    And I should see Offline media
    And I should see Unique Calls wheel

  @selenium @invoca @ready
  Scenario: Invoca
    Given HS Login Page
    When I Log In with login "admin" and password "admin123"
    And I set date range from "11/01/2016" to "11/30/2016"
    And I click on invoca detail view
    Then I should see calls in detail view

  @selenium @error @ready
  Scenario: Error login
    Given HS Login Page
    And I see login page elements
    When I Log In with login "wronglogin" and password "wrongpass"
    Then I should see error message
    When I Log In with login "admin" and password "admin123"
    Then I should see Logo and navigation bar

  @selenium @smoke
  Scenario: UI Validation
    Given HS Login Page
    When I Log In with login "admin" and password "admin123"
    And I set date range from "11/01/2016" to "11/30/2016"
    And I click on invoca detail view
    Then I should see calls in detail view
    And I should see Logo and navigation bar
    And I should see Title and date selector
    And I should see summary widgets
    And I should see Paid online media
    And I should see Impressions
    And I should see Performance metrics
    And I should see Facebook ads
    And I should see Offline media
    And I should see Unique Calls wheel