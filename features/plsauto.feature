Feature: Smoketest

  @selenium @Home @smoketest
  Scenario: Homepage validation
    Given PLS AUTO website
    And I should see all UI objects

  @selenium @Type @smoketest
  Scenario: Filter by Type
    Given PLS AUTO website
    When I click "Type"
    And I select Mid-Size MPV
    Then I should see Mid-size MPV cars

  @selenium @Make @smoketest
  Scenario: Filter by Make
    Given PLS AUTO website
    When I click "Make"
    And I select Toyota
    Then I should see Toyota cars

  @selenium @Search @smoketest
  Scenario: Search functionality
    Given PLS AUTO website
    When I search Toyota
    Then I should see Toyota cars

  @javascript @Searchjs
  Scenario: Search functionality
    Given PLS AUTO website
    When I search Toyota
    Then I should see Toyota cars

