# frozen_string_literal: true
require 'rubygems'
require 'bundler/setup'
require 'capybara'
require 'cucumber'
require 'open-uri'
require 'capybara/cucumber'
require 'capybara/spec/test_app'
require 'capybara/poltergeist'
require 'capybara-screenshot/cucumber'

###############################headless poltergeist browser support
Capybara.javascript_driver = :poltergeist
Capybara.register_driver :poltergeist do |app|

Capybara::Poltergeist::Driver.new(app,
        :phantomjs_options => ['--debug=no', '--load-images=no', '--ignore-ssl-errors=yes', '--ssl-protocol=TLSv1'],
        :debug => false,
        :js_errors => false)
end
Capybara.app = TestApp

###############################selenium driver support
Capybara.default_driver = :selenium
Capybara.app_host = "http://www.google.com"
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new app, browser: :chrome
end

World(Capybara::DSL)

##############################custom browser window
Before '@selenium' do
  page.driver.browser.manage.window.resize_to(1600, 900)
end

#Capybara.default_max_wait_time = 5