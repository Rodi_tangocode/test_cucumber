Feature: Data Validator v1
  As a user
  I want to have proper data

  Background: Log In
    Given HS Login Page
    And I Log In with login "admin" and password "admin123"

  @selenium @total
  Scenario Outline: Total count
    When I set date range from "<START DATE>" to "<END DATE>"
    Then I should see "<TOTAL SPEND>" in Total Spend $
    And I should see "<TOTAL LEADS>" in Total Leads
    And I should see "<TOTAL CPL>" in Total CPL $

    Examples:
  | START DATE |  END DATE    | TOTAL SPEND  | TOTAL LEADS  | TOTAL CPL |
  | 09/01/2016 |  09/30/2016  |   28,683     |     1,190    |     24    |



  @javascript @paidonline
  Scenario Outline: Paid online media validation
    When I set date range from "<START DATE>" to "<END DATE>"
    Then I should see "<COST>" in Cost $
    And I should see "<P.P.COST>" in Previous Period Cost $
    And I should see "<CLICKS>" in Clicks
    And I should see "<P.P.CLICKS>" in Previous Period Clicks
    And I should see "<CPC>" in Cost Per Click $
    And I should see "<P.P.CPC>" in Previous Period Cost Per Click $
    And I should see "<CTR>" in Click-Through rate %
    And I should see "<P.P.CTR>" in Previous Period Click-Through rate %

    Examples:
| START DATE |  END DATE  | COST   | P.P.COST  | CLICKS | P.P.CLICKS |  CPC  | P.P.CPC | CTR  | P.P.CTR |
| 09/01/2016 | 09/30/2016 | 11,254 |  10,725   |  3,127 |   2,994    |  3.60 |  3.58   | 7.20 |  6.71   |
| 10/01/2016 | 10/31/2016 | 11,032 |  11,254   |  2,957 |   3,127    |  3.73 |  3.60   | 6.95 |  7.20   |
| 11/01/2016 | 11/30/2016 | 13,073 |  10,756   |  3,460 |   2,876    |  3.78 |  3.74   | 7.45 |  7.01   |
| 12/01/2016 | 12/31/2016 | 11,146 |  13,073   |  3,184 |   3,460    |  3.50 |  3.78   | 8.71 |  7.45   |


  @selenium @offlinemedia
  Scenario Outline: Offline media validation for whole year
    When I set date range from "<START DATE>" to "<END DATE>"
    Then I should see cost $ "<PRINT>" in Print
    And I should see cost $ "<RADIO>" in Radio
    And I should see cost $ "<TV>" in TV
    And I should see cost $ "<OUTDOOR>" in Outdoor

    Examples:
      | START DATE |  END DATE  | PRINT | RADIO |   TV   |  OUTDOOR |
      | 01/01/2016 | 01/31/2016 |  850  | 1,150 |  1,810 |   610    |
      | 02/01/2016 | 02/29/2016 |  730  | 1,160 |  1,990 |   600    |
      | 03/01/2016 | 03/31/2016 |  600  | 1,250 |  2,510 |   540    |
      | 04/01/2016 | 04/30/2016 |  950  | 1,330 |  3,000 |   456    |
      | 05/01/2016 | 05/31/2016 |  821  | 1,543 |  2,210 |   789    |
      | 06/01/2016 | 06/30/2016 |  840  | 1,234 |  1,710 |   678    |
      | 07/01/2016 | 07/31/2016 |  660  |  950  |  2,770 |   555    |
      | 08/01/2016 | 08/31/2016 |  899  | 2,000 |  1,933 |   777    |
      | 09/01/2016 | 09/30/2016 |  950  | 2,000 |  1,989 |   1,234  |
      | 10/01/2016 | 10/31/2016 |  670  | 1,150 |  2,016 |   1,111  |
      | 11/01/2016 | 11/30/2016 |  777  | 1,750 |  2,230 |   700    |
      | 12/01/2016 | 12/31/2016 |  850  | 1,109 |  1,210 |   800    |